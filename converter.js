/* eslint-disable linebreak-style */
/**
 * @module converter
 */

const { JsonLdBuilder } = require('jsdoc-plugin-rdf/converter/JsonLdBuilder');

let keyMap;
let splitCapNode;
let flat;

function converterSettings(conf) {
  keyMap = conf.vocabMap;
  splitCapNode = conf.splitCapNode;
  flat = conf.flattenedFormat;
}

function convertDescNode(graphList) {
  const descNode = graphList.find((node) => node[keyMap.potentialAction]);
  let returnList = graphList;
  if (!flat && descNode && splitCapNode) {
    returnList = graphList.filter((node) => node !== descNode);
    const funcNode = { ...descNode[keyMap.potentialAction] };
    delete descNode[keyMap.potentialAction][keyMap.params];
    delete descNode[keyMap.potentialAction][keyMap.returns];
    delete funcNode['@type'];
    returnList.unshift(descNode, funcNode);
  }
  return returnList;
}

/*
 * adjust a node right after it was added to the result
 * @param {object} graphNode
 * @returns the (changed) node
function extendConvert(graphNode) {
  const returnNode = graphNode;
  const key = '..';
  if (graphNode[key]) {

    returnNode[key] = '..';
  }
  return returnNode;
} */

function convert(genericRdf) {
  const builder = new JsonLdBuilder(genericRdf);
  builder.subjects.forEach((subjectId) => {
    let graphNode;
    if (!builder.flattenedFormat) {
      graphNode = builder.convertNested(subjectId);
    } else {
      graphNode = builder.convertFlat(subjectId);
    }
    if (graphNode) {
      // graphNode = extendConvert(graphNode);
      builder.jsonLd['@graph'].push(graphNode);
    }
  });
  builder.jsonLd['@graph'] = convertDescNode(builder.jsonLd['@graph']);
  return builder.jsonLd;
}

module.exports = {
  converterSettings,
  convert,
};
