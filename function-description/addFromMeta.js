/* eslint-disable linebreak-style */
const { getMembersFunctionName } = require('jsdoc-plugin-rdf/utils');

let subjectLong;

function addObjectProps(doc, currentRdf, parentId, tagtitle) {
  const propIdList = [];
  const rdf = currentRdf;
  const valueObj = JSON.parse(doc.meta.code.value);
  // doc.meta.code.value: {\"encodingFormat\":\"image/png\",\"gps\":\"\"}
  Object.keys(valueObj).forEach((key) => {
    let propId = key;
    if (subjectLong) {
      propId = `${parentId}.${key}`;
    }
    propIdList.push(`_:${propId}`);
    rdf.addSubject(propId);
    if (valueObj[key] === '') { // \"gps\":\"\" -> gps has object value
      /* marker to do again for nested object, its doclet will be parsed later
        and if it has a graph node already this property will be checked */
      rdf.addTriple(propId, 'addObjectProps', tagtitle);
    } else {
      let typeList = [];
      if (doc[tagtitle]) {
        typeList = doc[tagtitle].type ? doc[tagtitle].type.names : [];
      }
      let typeOf = typeof valueObj[key];
      if (typeOf === 'number') {
        typeOf = Number.isInteger(valueObj[key]) ? 'integer' : typeOf;
      }
      rdf.addTriple(propId, 'type', [...typeList, typeOf]);
      rdf.addTriple(propId, 'defaultvalue', valueObj[key]);
      if (rdf.graph[parentId] && rdf.graph[parentId].addObjectProps) { // remove marker
        delete rdf.graph[parentId].addObjectProps;
      }
    }
  });
  return propIdList;
}

/**
   * @param {object} doclet
   * @param {string} tagtitle - doclet key that has the id that should get the props
   * @param {genericRdf} currentRdf
   */
function addPropsFromTag(doclet, tagtitle, currentRdf) {
  const doc = doclet;
  doc.undocumented = true;
  const rdf = currentRdf;
  let tagName = doc[tagtitle];
  if (tagName) {
    tagName = doc[tagtitle].name ? doc[tagtitle].name : tagName;
  }
  let objId = tagName || doc.name;
  if (subjectLong) {
    objId = `${doc.memberof}.${objId}`;
  }
  if (objId === 'return') {
    const funcName = getMembersFunctionName(doc);
    objId = `${funcName}.returns`;
  }
  if (doc.meta && doc.meta.code.value !== undefined) {
    const propIdList = addObjectProps(doc, rdf, objId, tagtitle);
    rdf.addSubject(objId);
    if (propIdList.length > 0) {
      rdf.addTriple(objId, 'hasPart', propIdList);
    }
  }
}

function addFromMeta(doclet, rdf, subjectId) {
  if (doclet.meta.code.type === 'FunctionDeclaration') {
    doclet.meta.code.paramnames.forEach((pname) => {
      rdf.addSubject(subjectId);
      rdf.addTriple(subjectId, 'params', `_:${pname}`);
      rdf.addSubject(pname);
      rdf.addTriple(pname, 'type', 'fno:Parameter');
    });
  } else if (doclet.meta.code.type === 'ObjectExpression') {
    addPropsFromTag(doclet, subjectId, rdf);
  }
}

module.exports = {
  addPropsFromTag,
  addFromMeta,
};
