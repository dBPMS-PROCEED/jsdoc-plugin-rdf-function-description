/* eslint-disable linebreak-style */
/**
 * @module tags
 */

const { addValueToPred, defineTagFromConfig } = require('jsdoc-plugin-rdf/tags/defineTagUtils');
const { getLastElement } = require('jsdoc-plugin-rdf/utils');

let errorType;
let returnTag;
let callbackParamTag;
let callbackReturnTag;
let kindToType;
let includeMdnParts;

function funcTagSettings(tags, err, encoder) {
  callbackParamTag = tags.callbackParamTag;
  callbackReturnTag = tags.callbackReturnTag;
  returnTag = tags.returnTag;
  errorType = err;
  kindToType = encoder.kindToType;
  includeMdnParts = encoder.includeMdnParts;
}

/* function typePromise(doclet, tag) {
  if (tag.value && tag.value.type && tag.value.type.names.includes('mdn:Promise')) {
    doclet.promise = true;
  } else if (doclet.promise && tag.value.name.includes('.')) {
    const promiseNode = getLastElement(doclet.returns);
    if (tag.value.type === errorType) {

    }
  }
    /* const resolved = [{
      name: 'promiseResolved',
    }, {
      name: 'promiseResolved.resultValue',
      type: tag.value.name,
    }];
    const rejected = [{
      name: 'promiseRejected',
    }, {
      name: 'promiseRejected.resultError',
      type: errorType,
    }];
    const doc = doclet;
    if (!doclet.returns) {
      doc.returns = [...rejected, ...resolved];
    } else {
      doc.returns = [...rejected, ...resolved, ...doclet.returns];
    }
  }
} */

function errorParts(predKey, tag) {
  const subjId = tag.value.name || `${predKey}Error`;
  const parts = [{
    name: `${subjId}.message`,
    type: 'string',
  }, {
    name: `${subjId}.name`,
    type: 'string',
  }];
  // const doc = doclet;
  // doc[predKey] = [...doclet[predKey], ...errorParts];
  return parts;
}

function isType(tagvalue, typename) {
  let bool = false;
  if (tagvalue.type && tagvalue.type.names) {
    bool = tagvalue.type.names.some(
      (name) => [typename, kindToType[typename]].includes(name),
    );
  }
  return bool;
}

function callbackParamReturn(doclet, tag, key, subjKey) {
  let added = false;
  const cbs = doclet.params.filter((p) => isType(p, 'function'));
  if (cbs.length > 0) {
    const cb = getLastElement(cbs);
    if (tag.value.name && !tag.value.name.includes('.')) { // don't have to add parts ref
      addValueToPred(cb, key, [`_:${tag.value.name}`]); // id ref in last cb node
    }
    const tagval = tag.value;
    if (includeMdnParts && isType(tag.value, errorType)) {
      tagval.hasPart = errorParts(subjKey, tag);
    }
    // for next desc tags to find their subject: conf.subjectProps cbParams or cbReturn
    addValueToPred(doclet, subjKey, [tagval]);
    added = true;
  }
  return added;
}

// if callback param tag is custom, when used can assume '@param {function}' was defined before
const cbParamTagOptions = {
  mustHaveValue: true,
  canHaveName: true,
  canHaveType: true,
  onTagged: (doclet, tag) => {
    callbackParamReturn(doclet, tag, 'params', 'cbParam');
  },
};

const cbReturnTagOptions = {
  mustHaveValue: true,
  canHaveName: true,
  canHaveType: true,
  onTagged: (doclet, tag) => {
    callbackParamReturn(doclet, tag, 'returns', 'cbReturn');
  },
};

/* if there is a function type param, add param ref to last, else just to doclet params
const paramTagOptions = {
  mustHaveValue: true,
  canHaveName: true,
  canHaveType: true,
  onTagged: (doclet, tag) => {
    if (doclet.params) {
      if (!isType(tag.value, 'function')) {
        if (!callbackParamReturn(doclet, tag, 'params', 'cbParam')) {
          addValueToPred(doclet, 'params', tag.value);
          // typeError(doclet, 'params', tag);
        }
      } else {
        addValueToPred(doclet, 'params', tag.value);
      }
    } else {
      const doc = doclet;
      doc.params = [tag.value];
      // typeError(doclet, 'params', tag);
    }
    if (isType(tag.value, errorType)) {
      typeError(doclet, 'params', tag);
    }
  },
}; */

const returnTagOptions = { // todo: distinguish only description from name by '-'
  canHaveName: true,
  canHaveType: true,
  onTagged: (doclet, tag) => {
    if (tag.value) {
      // typePromise(doclet, tag);
      const tagval = tag.value;
      if (includeMdnParts && isType(tag.value, errorType)) {
        tagval.hasPart = errorParts('returns', tag);
      }
      // don't add properties to 'returns' so only parent shows in docu
      if (tag.value.name && doclet.returns && tag.value.name.split('.')[1]) {
        const properties = tag.value.name.split('.');
        const childId = properties[properties.length - 1];
        const parentId = tag.value.name.split(`.${childId}`)[0];
        const parentlist = doclet.returnPart
          ? [...doclet.returns, ...doclet.returnPart] : doclet.returns;
        const parent = parentlist.find((rt) => rt.name === parentId);
        // returnpart just node id for follow prop tags
        if (parent) {
          tagval.name = childId;
          addValueToPred(doclet, 'returnPart', [{ name: childId }]); // for property tags
          addValueToPred(parent, 'hasPart', [tagval]);
        }
      } else {
        if (tag.value.name) { // for documentation
          tagval.description = tag.value.description ? `${tag.value.name} ${tag.value.description}` : tag.value.name;
        }
        addValueToPred(doclet, 'returns', [tagval]);
      }
    }
  },
};

function functionTagDefinitions(dictionary) {
  defineTagFromConfig(dictionary, returnTag, returnTagOptions);
  defineTagFromConfig(dictionary, callbackReturnTag, cbReturnTagOptions);
  defineTagFromConfig(dictionary, callbackParamTag, cbParamTagOptions);
}
/* if (['param', 'arg', 'argument'].includes(callbackParamTag)) {
    defineTagFromConfig(dictionary, callbackParamTag, paramTagOptions);
  } else { */
module.exports = {
  funcTagSettings,
  functionTagDefinitions,
};
