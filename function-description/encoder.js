/* eslint-disable linebreak-style */
/**
 * @module encoder
 */

const { removeHtml } = require('jsdoc-plugin-rdf/utils');
const { buildFunctionMapping } = require('./functionMapping');
const { addFromMeta, addPropsFromTag } = require('./addFromMeta');

let defaultPropsFromTag;
let includeUndocumented;
let rdfConditionKey;

function encoderSettings(propsTag, include, createFor) {
  defaultPropsFromTag = propsTag;
  includeUndocumented = include;
  rdfConditionKey = (createFor && createFor.hasKey) ? createFor.hasKey : false;
}

/**
 * get potential action from function doclet type
 * @param {object} doc doclet to create new node from
 * @param {genericRdf} currentRdf genericRdf instance that contains function doclet info
 */
function addPotentialActionNode(doc, currentRdf) {
  const rdf = currentRdf;
  const functionId = doc.name;
  const descNode = { potentialAction: `_:${functionId}` };
  let descId = `${functionId}Description`;
  if (doc[rdfConditionKey].name) {
    descId = doc[rdfConditionKey].name;
  }
  rdf.addNode(descId, descNode);
}

// for doclet only, not its property objects
function setSubjectExtension(doclet, rdf, subjectId) {
  let result = subjectId;

  if (rdfConditionKey && doclet[rdfConditionKey]) {
    addPotentialActionNode(doclet, rdf);

    if (subjectId && doclet.kind === 'function') {
      rdf.addSubject('functionMapping');
      rdf.addTriple('functionMapping', 'node', [{ id: subjectId, filename: doclet.meta.filename }]);
    }
  }
  // todo: make argsId functionanme.argsId in addProps functions?
  // and propIds argsId.propId

  if (doclet.cbParam) {
    const cbs = doclet.params.filter((p) => p.params);
    const nohtmlp = [];
    const nohtmlr = [];
    cbs.forEach((cb) => {
      const cbNode = cb;
      cb.params.forEach((pref) => {
        nohtmlp.push(removeHtml(pref));
      });
      if (cb.returns) {
        cb.returns.forEach((rref) => {
          nohtmlr.push(removeHtml(rref));
        });
      }
      cbNode.params = nohtmlp;
      cbNode.returns = nohtmlr;
    });
  }
  if (defaultPropsFromTag && doclet[defaultPropsFromTag]) {
    const tagtitle = defaultPropsFromTag;
    addPropsFromTag(doclet, tagtitle, rdf);
    result = false;
  } // if property object had nested objects: graph node for it was added
  if (!includeUndocumented && rdf && rdf.graph[doclet.name] && rdf.graph[doclet.name].addObjectProps) {
    if (doclet.meta && doclet.meta.code.type === 'ObjectExpression') {
      const tagtitle = rdf.graph[doclet.name].addObjectProps;
      addPropsFromTag(doclet, tagtitle, rdf);
    }
    result = false;
  } // todo: includeMeta option to do also if not includeUndocumented
  if (includeUndocumented && doclet.meta) {
    addFromMeta(doclet, rdf, subjectId);
    result = false;
  }
  return result;
}

/**
 * to execute after doclet and properties have been added to genericRdf
 * @param {object} id id of the subject that has been parsed by rdfEncoder
 * @param {GenericRdf} currentRdf instance that contains the doclets properties
 */
function convertExtension(id, currentRdf) {
  const rdf = currentRdf;
  if (rdf.graph[id].cbParam) {
    // subj nodes for params have been made, refs in cb param node
    delete rdf.graph[id].cbParam;
  }
  if (rdf.graph[id].cbReturn) {
    delete rdf.graph[id].cbReturn;
  }
  // for returns
  if (rdf.graph[id].returnPart) {
    rdf.subjectDouble = rdf.subjectDouble.filter((str) => !rdf.graph[id].returnPart.includes(`_:${str}`));
    delete rdf.graph[id].returnPart;
  }
  if (rdf.graph[id].description) {
    if (rdf.graph[id].description === id) {
      delete rdf.graph[id].description;
    } else {
      const desc = rdf.graph[id].description.split(`${id} `);
      rdf.graph[id].description = (desc.length === 2) ? desc[1] : rdf.graph[id].description;
    }
  }
}

/**
 * use the finished rdf instance to build further graph nodes like function mapping
 * @param {genericRdf} rdf the finished rdf instance
 */
function beforeConvert(rdf) {
  const rdfGraph = rdf.graph;
  if (rdfGraph.functionMapping) {
    rdfGraph.functionMapping.node.forEach((funcNode) => {
      buildFunctionMapping(funcNode, rdf);
    });
    delete rdfGraph.functionMapping;
  }
}

module.exports = {
  encoderSettings,
  setSubjectExtension,
  convertExtension,
  beforeConvert,
};
