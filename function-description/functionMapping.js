/* eslint-disable linebreak-style */
/**
 * @module functionMapping
 */

const { addToPred } = require('jsdoc-plugin-rdf/utils');

let implementationType;
let paramMappingType;
let errorType;
let propSep;

function funcMappingSettings(conf, err) {
  implementationType = conf.implementationType;
  paramMappingType = conf.paramMappingType;
  errorType = err;
  propSep = conf.propSeparator;
}

function addImplementationNode(funcNode, currentRdf) {
  const rdf = currentRdf;
  const implementationId = `${funcNode.id}Implementation`;
  const implementationNode = {
    type: implementationType,
    filename: funcNode.filename,
  };
  rdf.addNode(implementationId, implementationNode);
  return implementationId;
}

function returnTypeCheck(nodeType) {
  const returnType = nodeType;
  const returnTypes = Array.isArray(returnType) ? returnType : [returnType];
  return !returnTypes.includes(errorType) ? 'DefaultReturnMapping' : 'ExceptionReturnMapping';
}

function noBlankNode(id) {
  let trimmedId = id;
  if (id.split('_:').length === 2) {
    // eslint-disable-next-line prefer-destructuring
    trimmedId = id.split('_:')[1];
  }
  return trimmedId;
}

function toBlankNode(str) {
  return `_:${str}`;
}

/**
   * adds mapping nodes from a node's properties list
   * @param {string} nodeId id of the graph node to check for properties
   * @param {object} mappingParentNode the mapping node of nodeId (needed to add to it)
   * @param {GenericRdf} currentRdf instance to add graph nodes to
   * @param {array} parentMappingIds ids already collected in returnParamMapping
   * @returns {array} mapping node ids with eventual added ids from nodeId.hasPart
   */
function addMappingProperties(nodeId, mappingParentNode, currentRdf, parentMappingIds) {
  let mappingIds = parentMappingIds;
  const rdf = currentRdf;
  const graphNode = rdf.graph[noBlankNode(nodeId)];
  if (graphNode.hasPart) {
    const propIds = Array.isArray(graphNode.hasPart)
      ? graphNode.hasPart : [graphNode.hasPart];
    propIds.forEach((id) => {
      const propId = noBlankNode(id);
      const propMappingNode = { ...mappingParentNode };
      if (mappingParentNode.functionParameter) {
        propMappingNode.functionParameter = addToPred(mappingParentNode.functionParameter, id);
      } else if (mappingParentNode.functionOutput) {
        propMappingNode.type = mappingParentNode.type === 'ExceptionReturnMapping'
          ? 'ExceptionReturnMapping' : returnTypeCheck(rdf.graph[propId].type);
        propMappingNode.functionOutput = addToPred(mappingParentNode.functionOutput, id);
      }
      if (mappingParentNode.implementationProperty) {
        propMappingNode.implementationProperty = `${mappingParentNode.implementationProperty}${propSep}${propId}`;
      }
      rdf.addNode(`${propId}PropMapping`, propMappingNode);
      mappingIds.push(`${id}PropMapping`);
      mappingIds = addMappingProperties(propId, propMappingNode, rdf, mappingIds);
    });
  }
  return mappingIds;
}

function paramMapping(funcId, currentRdf) {
  const rdf = currentRdf;
  let mappingIds = [];
  let position = 0;
  let paramIds = rdf.graph[funcId].params;
  paramIds = Array.isArray(paramIds) ? paramIds : [paramIds];
  paramIds.forEach((id) => {
    const param = noBlankNode(id);
    const paramNode = {
      // todo: if position param tag can have no name?
      type: paramMappingType,
      functionParameter: id,
    };
    if (paramNode.type === 'PositionParameterMapping') {
      position += 1;
      paramNode.implementationParameterPosition = position;
    } else {
      paramNode.implementationProperty = param;
    }
    rdf.addNode(`${param}ParamMapping`, paramNode);
    mappingIds.push(`${id}ParamMapping`);
    mappingIds = addMappingProperties(param, paramNode, rdf, mappingIds);
  });
  return mappingIds;
}

function returnMapping(funcId, currentRdf) {
  const rdf = currentRdf;
  const funcNode = rdf.graph[funcId];
  let returnMapIds = [];
  if (funcNode.returns) {
    const returnIds = addToPred(funcNode.returns, []);
    returnIds.forEach((id) => {
      const returnId = noBlankNode(id);
      const mappingNode = {
        type: returnTypeCheck(rdf.graph[returnId].type),
        functionOutput: id,
      };
      if (!returnId.includes(`${funcId}.returns`)) { // return tag had a name
        mappingNode.implementationProperty = returnId;
      }
      rdf.addNode(`${returnId}ReturnMapping`, mappingNode);
      returnMapIds.push(`${id}ReturnMapping`);
      returnMapIds = addMappingProperties(returnId, mappingNode, rdf, returnMapIds);
    });
  }
  return returnMapIds;
}

/**
   * build function mapping from generic rdf info
   * @param {object} funcIdFile has properties id (function name) and filename
   * @param {GenericRdf} currentRdf finished instance
   */
function buildFunctionMapping(funcIdFile, currentRdf) {
  const rdf = currentRdf;
  const funcName = funcIdFile.id;
  const implementationId = addImplementationNode(funcIdFile, rdf);

  // function mapping node
  const funcMapId = `${funcName}Mapping`;
  const funcMapNode = {
    type: 'mapping',
    mapfunction: toBlankNode(funcName),
    implementation: toBlankNode(implementationId),
  };
  rdf.addNode(funcMapId, funcMapNode);

  const paramMapIds = paramMapping(funcName, rdf);
  const returnMapIds = returnMapping(funcName, rdf);

  /* todo: methodNode optional? give type in function as tag @methodMapping {string}?
    const methodMapId = `${funcName}MethodMapping`;
    const methodMapNode = {
      type: 'stringMethodMapping',
      methodName: funcName,
    };
    rdf.addNode(methodMapId, methodMapNode);
    rdf.addTriple(funcMapId, 'methodMapping', methodMapId); */

  if (paramMapIds.length > 0) {
    rdf.addTriple(funcMapId, 'parameterMapping', paramMapIds);
  }
  if (returnMapIds.length > 0) {
    rdf.addTriple(funcMapId, 'returnMapping', returnMapIds);
  }
}

module.exports = {
  funcMappingSettings,
  buildFunctionMapping,
};
