/**
* @LinkedDataDescription
* @type schema:PhotographAction
*
* @param args
* @param {schema:Integer} args.h
* @predicate {schema:height}
* @unit px
*
* @param {schema:Integer} args.w
* @predicate {schema:width}
* @unit px
* @maxValue 20
*
* @param {schema:Integer} [args.dpi] - required Dots Per Inch value, resolution
* @predicate {dpi}
*
* @param [args.options]
* @predicate {options}
* @param {schema:Boolean} [args.options.blackWhite=false]
* @predicate {blackWhite}
* @param {schema:Integer} [args.options.rotation=0]
* @predicate {rotation}
* @minValue 0
* @maxValue 359
*
* @param {function|fno:Output} [callback]
* @cbParam {mdn:Error} error
* @cbParam img
* @predicate {schema:ImageObject}
* @cbParam img.gps
* @predicate {schema:GeoCoordinates}
* @cbParam {schema:Float} gps.lat
* @predicate {schema:latitude}
* @cbParam {schema:Float} gps.long
* @predicate {schema:longitude}
* @cbParam img.photo
* @predicate {schema:Photograph}
* @encodingFormat image/png
* @encoding base64
*/
function PhotographActionCb(args, callback) {
}

// callback opt 2
  /*
  * @param {mdn:Error} err
  * @param img
  */
function callback(err, img) {};