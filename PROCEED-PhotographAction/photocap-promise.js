/**
* @LinkedDataDescription
* @type schema:PhotographAction
*
* @param args
* @param {schema:Integer} args.h
* @predicate {schema:height}
* @unit px
*
* @param {schema:Integer} args.w
* @predicate {schema:width}
* @unit px
* @maxValue 20
*
* @param {schema:Integer} [args.dpi] - required Dots Per Inch value, resolution
* @predicate {dpi}
*
* @param [args.options]
* @predicate {options}
* @param {schema:Boolean} [args.options.blackWhite=false]
* @predicate {blackWhite}
* @param {schema:Integer} [args.options.rotation=0]
* @predicate {rotation}
* @minValue 0
* @maxValue 359
* 
* @return {mdn:Promise} imgObject object with image properties
* @return {mdn:Error} imgObject.error
* @return imgObject.img
* @predicate {schema:ImageObject}
* @return img.gps
* @predicate {schema:GeoCoordinates}
* @return {schema:Float} gps.lat
* @predicate {schema:latitude}
* @return {schema:Float} gps.long
* @predicate {schema:longitude}
* @return img.photo
* @predicate {schema:Photograph}
* @encodingFormat image/png
* @encoding base64
*/
function PhotographActionPromise(args) {  
}
