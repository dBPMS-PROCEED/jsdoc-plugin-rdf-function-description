// callback example 1
/**
 * @LinkedDataDescription
 * @param args
 * @param {fno:Function} [callback1]
 * @cbParam {schema:ImageObject} img
 * @cbReturn img
 */
function callbackExample1(args, callback1) {}

// callback example 2
/**
 * @LinkedDataDescription cbExample2
 * @param callback2
 */
function callbackExample2(callback2) {}

/**
 * @param {schema:ImageObject} img
 */
function callback2(img) {}
