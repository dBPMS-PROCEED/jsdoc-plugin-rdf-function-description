/**
* @LinkedDataDescription
* @type schema:PhotographAction
*
* @param args
* @param args.height=200
* @predicate {schema:height}
* @unit px
*
* @return {schema:ImageObject}
*/
function takePhoto(args) {
}