# jsdoc-plugin-rdf-function-description

This plugin can be used to generate a Linked Data description in JSON-LD format of a function implementation by writing JSDoc comments. It extends [jsdoc-plugin-rdf](https://gitlab.com/dBPMS-PROCEED/jsdoc-plugin-rdf).

# Installation

Use the following command in the root directory where JSDoc is installed

```
npm install jsdoc-plugin-rdf-function-description
```

# Add plugin to JSDoc
In JSDoc's configuration file add the path to the plugin

```
...
"plugins": [
    "node_modules/jsdoc-plugin-rdf-function-description"
],
```

# Usage

A JSON-LD description will be generated for each function with a `@LinkedDataDescription` tag. Example `js` files with their JSON-LD results can be found in the [examples](https://gitlab.com/dBPMS-PROCEED/jsdoc-plugin-rdf-function-description/tree/master/examples) directory.

RDF subjects are created for `function`, parameter (`@param`), callback parameter (`@cbParam`), return (`@return`) and callback return (`@cbReturn`) descriptions. Each subject can be described further with subsequent tags. A `@param` with type `function` or `fno:Function` will be the subject for `@cbParam` and `@cbReturn` tags following it. An example for callback descriptions can be found under the examples directory as well.

The return tags can be described just like a parameter value with property parts.

Property tags must be added after the subject they are meant to describe.

# plugin configuration

The default settings are in `config.js`. They can be replaced with a `rdf.config.json` in the same directory as `index.js`.

## tags

```
"tagSettings": {
    "propertyTags": ["unit", "nullable"],
}
```
The tags defined with this option will become a property of the preceding subject (function, parameter or return). A property tag without a value i.e. `@nullable` will result in `nullable: true`. You can either add the tag titles with a vocabulary prefix (`fno:nullable`) or define them as above and use the `vocabMap` to add the prefix in the converter (described in the converter section).

## default vocabulary
The vocabulary that will always be added to the context is given in `encoderSettings`. You can change the `vocab` property.

```
"encoderSettings": {
    "vocab": {
     schema: 'https://schema.org/',
      fno: 'https://w3id.org/function/ontology#',
      xsd: 'http://www.w3.org/2001/XMLSchema#',
      'dbpedia-owl': 'http://dbpedia.org/ontology/',
      fnom: 'https://w3id.org/function/vocabulary/mapping#',
      fnoi: 'https://w3id.org/function/vocabulary/implementation',
      terms: 'http://purl.org/dc/terms/',
      mdn: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/',
    }
}
```

## converter

`vocabMap` adds a vocabulary prefix to JSDoc's default keys like params and description. You can add your own tag titles (defined in `tagSettings`) here.

```
"converterSettings": {
    "vocabMap": {
        "params": "fno:expects",
        "returns": "fno:returns",
        "description": "schema:description"
    },
    "flattenedFormat": true,
    "predNoNestedFormat": []
}
```

The default is JSON-LD in flattened document form. Set the option to `false` for the nodes to be nested or combine the two options by giving predicate names to `predNoNestedFormat` that should remain flattened in an otherwise nested format.

## output

A JSON-LD description will be printed for every file that is parsed by JSDoc. You can print all descriptions to one file by changing `printPerDesc` to `false`. The default output path is a `plugin-rdf-function-description` directory in JSDoc's destination. Change it by setting the `destination` directory (relative to JSDoc's destination).

```
"general": {
    "printPerDesc": true,
    "destination": "./plugin-rdf-function-description"
}
```

# Replace the converter

To change or replace the converter a `converter` module has to export a `convert` function that will get an intermediate RDF format of the form

```
{
    subject: {
        predicate: object
    }
}
```

as parameter.