/* eslint-disable linebreak-style */
/**
 * @module plugins/rdf-function-description
 */

const logger = require('jsdoc/util/logger');
const { sep, getLastElement } = require('jsdoc-plugin-rdf/utils');
const { printResults } = require('jsdoc-plugin-rdf/converter/print');
const { setDocletSubject, docletToRdf } = require('jsdoc-plugin-rdf/rdf/rdfEncoder');
const { convertDocletProperties } = require('jsdoc-plugin-rdf/rdf/rdfProperties');
const { rdfTagDefinitions } = require('jsdoc-plugin-rdf/tags/rdfTags');
const { functionTagDefinitions } = require('./function-description/tags');
const { setSubjectExtension, convertExtension, beforeConvert } = require('./function-description/encoder');
const { convert } = require('./converter');
const { setConfig } = require('./config');

const pluginname = 'plugin-rdf-function-description';

const configPath = () => {
  let path;
  try {
    path = require.resolve('./rdf.config.json');
  } catch (e) {
    console.log(`${pluginname}: using default config`);
  }
  return path;
};

setConfig(configPath());

function defineTags(dictionary) {
  rdfTagDefinitions(dictionary);
  functionTagDefinitions(dictionary);
}

/**
 * done for every doclet
 * @param {object} doc the current doclet
 * @param {GenericRdf} rdf current instance
 */
function convertDoclet(doc, rdf) {
  let subjectName = setDocletSubject(doc);
  subjectName = setSubjectExtension(doc, rdf, subjectName);

  if (subjectName && rdf.addSubject(subjectName)) {
    const addedIds = convertDocletProperties(doc, subjectName, rdf, []);
    addedIds.forEach((id) => {
      convertExtension(id, rdf);
    });
  }
}

const handlers = {
  processingComplete(e) {
    const rdfResult = {};
    e.doclets.forEach((doclet) => {
      if (doclet.name) {
        docletToRdf(rdfResult, doclet, convertDoclet);
      }
    });
    if (rdfResult.result) {
      const rdfs = rdfResult.result;
      const converted = [];
      Object.keys(rdfs).forEach((rdfKey) => {
        const rdfInstance = rdfs[rdfKey];
        beforeConvert(rdfInstance);
        const result = convert(rdfInstance);
        const filename = getLastElement(rdfKey.split(`${sep}`));
        converted.push([filename, result]);
        if (rdfInstance.undefinedTags.length > 0) {
          logger.warn(`${pluginname}: Undefined tags used in ${filename}: ${rdfInstance.undefinedTags}`);
        }
        if (rdfInstance.subjectDouble.length > 0) {
          logger.warn(`${pluginname}: The properties for subjects with Id ${rdfInstance.subjectDouble} were combined in ${filename}. Rename them if this was not intentional.`);
        }
      });
      printResults(converted);
    } else {
      logger.warn(`${pluginname}: No doclets were converted. Check if a create condition was set (config.encoderSettings.createFor)`);
    }
  },
};

module.exports = {
  defineTags,
  handlers,
};
