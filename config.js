/* eslint-disable linebreak-style */

const { config } = require('jsdoc-plugin-rdf/config');
const { funcTagSettings } = require('./function-description/tags');
const { encoderSettings } = require('./function-description/encoder');
const { funcMappingSettings } = require('./function-description/functionMapping');
const { converterSettings } = require('./converter');

const defaults = {
  general: {
    destination: 'plugin-rdf-function-description',
    printPerDesc: true,
    errorType: 'mdn:Error',
  },
  tagSettings: {
    vocabTags: [
      'vocab',
      'context',
      'prefix',
      'rdfns',
    ],
    subjectTag: 'subj',
    predicateTag: 'pred',
    objectTag: 'obj',
    predTags: [
      'LinkedDataDescription',
      'defaultProps',
    ],
    propertyTags: [
      'fno:type',
      'predicate',
      'unit',
      'minValue',
      'maxValue',
      'encoding',
      'encodingFormat',
      'nullable',
      'algorithm',
      'solves',
    ],
    metaTags: ['filename'],
    subjectProps: ['returnPart', 'returns', 'cbReturn', 'cbParam', 'params', 'rdfsubject'],
    remainNode: ['LinkedDataDescription'],
    renameSubject: 'subjectId',
    defaultPropsFromTag: 'defaultProps',
    returnTag: 'return',
    callbackParamTag: 'cbParam',
    callbackReturnTag: 'cbReturn',
  },
  encoderSettings: {
    createFor: { hasKey: 'LinkedDataDescription' },
    allowDocletRef: true,
    vocab: {
      schema: 'https://schema.org/',
      fno: 'https://w3id.org/function/ontology#',
      xsd: 'http://www.w3.org/2001/XMLSchema#',
      'dbpedia-owl': 'http://dbpedia.org/ontology/',
      fnom: 'https://w3id.org/function/vocabulary/mapping#',
      fnoi: 'https://w3id.org/function/vocabulary/implementation',
      terms: 'http://purl.org/dc/terms/',
      mdn: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/',
    },
    excludeKeys: [
      'comment',
      'undocumented',
      'longname',
      'name',
      'meta',
      'memberof',
      'scope',
      'tags',
    ],
    kindToType: {
      function: 'fno:Function',
      params: 'fno:Parameter',
      cbParam: ['fno:Output', 'fno:Parameter'],
      cbReturn: 'fno:Output',
      returns: 'fno:Output',
      returnPart: 'fno:Output',
      solves: 'fno:Problem',
      algorithm: 'fno:Algorithm',
    },
    propertyCondition: [
      {
        required: true,
        conditions: {
          type: 'fno:Parameter',
          noKey: 'required',
        },
      },
      {
        params: [],
        conditions: {
          type: 'fno:Function',
        },
      },
      {
        returns: [],
        conditions: {
          type: 'fno:Function',
        },
      },
    ],
    // blankNodeIriLongname: false, todo?
    optionalToRequired: true,
    includeMdnParts: false,
    includeUndocumented: false,
  },
  converterSettings: {
    predNoNestedFormat: [],
    flattenedFormat: true,
    nestedIds: true,
    vocabMap: {
      // keys
      params: 'fno:expects',
      returns: 'fno:returns',
      algorithm: 'fno:implements',
      solves: 'fno:solves',
      filename: 'dbpedia-owl:filename',
      defaultvalue: 'schema:defaultValue',
      description: 'schema:description',
      potentialAction: 'schema:potentialAction',
      required: 'fno:required',
      nullable: 'fno:nullable',
      predicate: 'fno:predicate',
      hasPart: 'terms:hasPart',
      maxValue: 'schema:maxValue',
      minValue: 'schema:minValue',
      unit: 'schema:unitText',
      encodingFormat: 'schema:encodingFormat',
      // values
      function: 'fno:Function',
      integer: 'schema:Integer',
      boolean: 'schema:Boolean',
      string: 'schema:String',
      complexType: 'xsd:complexType',
      // function mapping keys
      mapfunction: 'fno:function',
      implementation: 'fno:implementation',
      methodMapping: 'fno:methodMapping',
      parameterMapping: 'fno:parameterMapping',
      returnMapping: 'fno:returnMapping',
      methodName: 'fnom:method-name',
      functionParameter: 'fnom:functionParameter',
      functionOutput: 'fnom:functionOutput',
      implementationProperty: 'fnom:implementationProperty',
      implementationParameterPosition: 'fnom:implementationParameterPosition',
      // function mapping values
      mapping: 'fno:Mapping',
      stringMethodMapping: 'fnom:StringMethodMapping',
      PositionParameterMapping: 'fnom:PositionParameterMapping',
      DefaultReturnMapping: 'fnom:DefaultReturnMapping',
      ExceptionReturnMapping: 'fnom:ExceptionReturnMapping',
    },
  },
  functionMapping: {
    implementationType: 'fnoi:JavaScriptFunction',
    paramMappingType: 'fnom:PropertyParameterMapping',
    propSeparator: '.',
  },
};

function setConfig(filepath) {
  const conf = config(filepath, defaults);
  funcTagSettings(
    conf.tagSettings,
    conf.general.errorType,
    conf.encoderSettings,
  );
  encoderSettings(
    conf.tagSettings.defaultPropsFromTag,
    conf.encoderSettings.includeUndocumented,
    conf.encoderSettings.createFor,
  );
  funcMappingSettings(conf.functionMapping, conf.general.errorType);
  converterSettings(conf.converterSettings);
}

module.exports = {
  setConfig,
};
